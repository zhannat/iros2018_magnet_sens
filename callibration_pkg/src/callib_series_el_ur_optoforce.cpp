#include <ros/ros.h>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/CalibWeissNano.h"
#include <geometry_msgs/WrenchStamped.h>
#include <arduino_connect_msgs/ser_el_sensor_read.h>
#include "tactile_servo_msgs/CalibSerieselOptoforce.h"

class CalibWeisNano

{
public:
  CalibWeisNano();
  ~CalibWeisNano();
  ros::Subscriber force;
  void callback_force(const geometry_msgs::WrenchStamped& msg);
  ros::Subscriber sub_feats_fb;
  void callback_fbfeats(const arduino_connect_msgs::ser_el_sensor_read& msg);
  ros::NodeHandle nh;
  ros::Publisher pub_calib;
  float fx, fy, fz, mx, my, mz;
  float fx_weiss, mx_weiss, my_weiss;
    float copx_weiss, copy_weiss, orient_weiss;
float cocx_weiss, cocy_weiss;
  bool is_feat_fb_rec_;
  std::vector<float> matrix;
};

CalibWeisNano::CalibWeisNano()
{
  force = nh.subscribe("/optoforce_0", 2, &CalibWeisNano::callback_force, this);
  sub_feats_fb = nh.subscribe("/series_elast_cells", 2,  &CalibWeisNano::callback_fbfeats,this);
  
  pub_calib = nh.advertise<tactile_servo_msgs::CalibSerieselOptoforce>("/callb_seriesel_optoforce", 2);
  is_feat_fb_rec_ = false;
  matrix.resize(16,0);
}
CalibWeisNano::~CalibWeisNano()
{}

void CalibWeisNano::callback_fbfeats(const arduino_connect_msgs::ser_el_sensor_read& msg_)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
    matrix[0] = msg_.adc0;
    matrix[1] = msg_.adc1;
    matrix[2] = msg_.adc2;
    matrix[3] = msg_.adc3;
    matrix[4] = msg_.adc4;
    matrix[5] = msg_.adc5;
    matrix[6] = msg_.adc6;
    matrix[7] = msg_.adc7;
    matrix[8] = msg_.adc8;
    matrix[9] = msg_.adc9;
    matrix[10] = msg_.adc10;
    matrix[11] = msg_.adc11;
    matrix[12] = msg_.adc12;
    matrix[13] = msg_.adc13;
    matrix[14] = msg_.adc14;
    matrix[15] = msg_.adc15;
    is_feat_fb_rec_ = true;
}

void CalibWeisNano::callback_force(const geometry_msgs::WrenchStamped& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
 
  fx = msg.wrench.force.x;
  fy = msg.wrench.force.y;
  fz =  std::abs( msg.wrench.force.z );
  mx = msg.wrench.torque.x;
  my = msg.wrench.torque.y;
  mz = msg.wrench.torque.z;
  
  if ( is_feat_fb_rec_ == true)
  {
    tactile_servo_msgs::CalibSerieselOptoforce calib;
    calib.header.frame_id = "weiss_callib";
    calib.header.stamp = ros::Time::now();
    calib.nano.force.x = fx;
    calib.nano.force.y = fy;
    calib.nano.force.z = fz;
    calib.nano.torque.x = mx;
    calib.nano.torque.y = my;
    calib.adc0 = matrix[0];
    calib.adc1 = matrix[1];
    calib.adc2 = matrix[2];
    calib.adc3 = matrix[3];
    calib.adc4 = matrix[4];
    calib.adc5 = matrix[5];
    calib.adc6 = matrix[6];
    calib.adc7 = matrix[7];
    calib.adc8 = matrix[8];
    calib.adc9 = matrix[9];
    calib.adc10 = matrix[10];
    calib.adc11 = matrix[11];
    calib.adc12 = matrix[12];
    calib.adc13 = matrix[13];
    calib.adc14 = matrix[14];
    calib.adc15 = matrix[15];
    
    ROS_DEBUG_STREAM("fb_feats_recieved");
    pub_calib.publish(calib);
    is_feat_fb_rec_ = false;
  }

}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "weiss_calib_cpp");
    ros::NodeHandle n;
    ros::Rate loop_rate(25);
    ROS_INFO("weiss_calib_cpp");
    CalibWeisNano weiss_callib;
    while( ros::ok() )
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


