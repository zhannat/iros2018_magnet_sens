#include <ros/ros.h>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

#include "tactile_servo_msgs/CalibWeissNano.h"

#include <geometry_msgs/WrenchStamped.h>

#include <geometry_msgs/PoseStamped.h>

#include "tactile_servo_msgs/PlanFeats.h"

#include "tactile_servo_msgs/PlotMatlabImg.h"

#include <geometry_msgs/PointStamped.h>

#include <sensor_msgs/Image.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <std_msgs/Float64.h>
#include <sensor_msgs/JointState.h>

class PlotMatlab

{
public:
  PlotMatlab();
  ~PlotMatlab();
     ros::Subscriber sub_joint_states;

    ros::Subscriber current_state_sub;
  void cb_current_state_sub(const std_msgs::Float64& msg);
  float current_state_;
  void jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg);
  std::vector<double> joint_values_;  
  std::vector<double> joint_efforts_;  
bool             is_recieved_new_joint_state_ ;

  bool is_in_contact_ur_;
  int pixels_in_contact_ur_;
  float highest_force_cell_ur_;
  float real_total_force_ur_;

  
  ros::Subscriber force;
  void callback_force(const geometry_msgs::WrenchStamped& msg);
  float fx, fy, fz, mx, my, mz;
  
  ros::Subscriber sub_feats_fb;
  void callback_fbfeats(const tactile_servo_msgs::ContsFeats& msg);
  float fx_weiss, mx_weiss, my_weiss, copx_weiss, copy_weiss, orient_weiss, cocx_weiss, cocy_weiss;
  
  float fx_weissd, mx_weissd, my_weissd, copx_weissd, copy_weissd, orient_weissd, cocx_weissd, cocy_weissd;
  
  int num_contours;
  
  float x, y, z, wx, wy, wz, ww;

//   ros::Subscriber pose_des;
//   void callback_posedes(const geometry_msgs::PoseStamped& msg);
  float xd, yd, zd, wxd, wyd, wzd, wwd;

  ros::Subscriber contacts_hist;
  void callback_contact_hist(const geometry_msgs::PointStamped& msg);
  float xc, yc, zc;
  ros::Subscriber contacts_hist1;
  void callback_contact_hist1(const geometry_msgs::PointStamped& msg);
  float xc1, yc1, zc1;
  ros::Subscriber contacts_hist2;
  void callback_contact_hist2(const geometry_msgs::PointStamped& msg);
  float xc2, yc2, zc2;
  ros::Subscriber contacts_hist3;
  void callback_contact_hist3(const geometry_msgs::PointStamped& msg);
  float xc3, yc3, zc3;
  ros::Subscriber contacts_hist4;
  void callback_contact_hist4(const geometry_msgs::PointStamped& msg);
  float xc4, yc4, zc4;
  ros::Subscriber contacts_hist5;
  void callback_contact_hist5(const geometry_msgs::PointStamped& msg);
  float xc5, yc5, zc5;
  ros::Subscriber contacts_hist6;
  void callback_contact_hist6(const geometry_msgs::PointStamped& msg);
  float xc6, yc6, zc6;
  ros::Subscriber contacts_hist7;
  void callback_contact_hist7(const geometry_msgs::PointStamped& msg);
  float xc7, yc7, zc7;
  ros::Subscriber contacts_hist8;
  void callback_contact_hist8(const geometry_msgs::PointStamped& msg);
  float xc8, yc8, zc8;
  ros::Subscriber contacts_hist9;
  void callback_contact_hist9(const geometry_msgs::PointStamped& msg);
  float xc9, yc9, zc9;
  ros::Subscriber contacts_hist10;
  void callback_contact_hist10(const geometry_msgs::PointStamped& msg);
  float xc10, yc10, zc10;
  ros::Subscriber contacts_hist11;
  void callback_contact_hist11(const geometry_msgs::PointStamped& msg);
  float xc11, yc11, zc11;
  ros::Subscriber contacts_hist12;
  void callback_contact_hist12(const geometry_msgs::PointStamped& msg);
  float xc12, yc12, zc12;
  ros::Subscriber contacts_hist13;
  void callback_contact_hist13(const geometry_msgs::PointStamped& msg);
  float xc13, yc13, zc13;
  ros::Subscriber contacts_hist14;
  void callback_contact_hist14(const geometry_msgs::PointStamped& msg);
  float xc14, yc14, zc14;
  ros::Subscriber contacts_hist15;
  void callback_contact_hist15(const geometry_msgs::PointStamped& msg);
  float xc15, yc15, zc15;
  
    ros::Subscriber contactsNO_hist;
  void callback_contactNO_hist(const geometry_msgs::PointStamped& msg);
  float xNOc, yNOc, zNOc;
  ros::Subscriber contactsNO_hist1;
  void callback_contactNO_hist1(const geometry_msgs::PointStamped& msg);
  float xNOc1, yNOc1, zNOc1;
  ros::Subscriber contactsNO_hist2;
  void callback_contactNO_hist2(const geometry_msgs::PointStamped& msg);
  float xNOc2, yNOc2, zNOc2;
  ros::Subscriber contactsNO_hist3;
  void callback_contactNO_hist3(const geometry_msgs::PointStamped& msg);
  float xNOc3, yNOc3, zNOc3;
  ros::Subscriber contactsNO_hist4;
  void callback_contactNO_hist4(const geometry_msgs::PointStamped& msg);
  float xNOc4, yNOc4, zNOc4;
  ros::Subscriber contactsNO_hist5;
  void callback_contactNO_hist5(const geometry_msgs::PointStamped& msg);
  float xNOc5, yNOc5, zNOc5;
  ros::Subscriber contactsNO_hist6;
  void callback_contactNO_hist6(const geometry_msgs::PointStamped& msg);
  float xNOc6, yNOc6, zNOc6;
  ros::Subscriber contactsNO_hist7;
  void callback_contactNO_hist7(const geometry_msgs::PointStamped& msg);
  float xNOc7, yNOc7, zNOc7;
  ros::Subscriber contactsNO_hist8;
  void callback_contactNO_hist8(const geometry_msgs::PointStamped& msg);
  float xNOc8, yNOc8, zNOc8;
  ros::Subscriber contactsNO_hist9;
  void callback_contactNO_hist9(const geometry_msgs::PointStamped& msg);
  float xNOc9, yNOc9, zNOc9;
  ros::Subscriber contactsNO_hist10;
  void callback_contactNO_hist10(const geometry_msgs::PointStamped& msg);
  float xNOc10, yNOc10, zNOc10;
  ros::Subscriber contactsNO_hist11;
  void callback_contactNO_hist11(const geometry_msgs::PointStamped& msg);
  float xNOc11, yNOc11, zNOc11;
  ros::Subscriber contactsNO_hist12;
  void callback_contactNO_hist12(const geometry_msgs::PointStamped& msg);
  float xNOc12, yNOc12, zNOc12;
  ros::Subscriber contactsNO_hist13;
  void callback_contactNO_hist13(const geometry_msgs::PointStamped& msg);
  float xNOc13, yNOc13, zNOc13;
  ros::Subscriber contactsNO_hist14;
  void callback_contactNO_hist14(const geometry_msgs::PointStamped& msg);
  float xNOc14, yNOc14, zNOc14;
  ros::Subscriber contactsNO_hist15;
  void callback_contactNO_hist15(const geometry_msgs::PointStamped& msg);
  float xNOc15, yNOc15, zNOc15;
  
  long double t_now_sec, t_begin_sec;
  long double t_now_nsec;
  ros::Duration dt, test_time;
  
  ros::Time t_old_sec_total, test_time_begin;
  long double n_cycle;
  long double test_time2;
  
  bool init_time;

  ros::NodeHandle nh;
  ros::Publisher pub_plot_matlab;
  
  bool is_feat_fb_rec_, is_feat_des_rec_,
  is_feat_plan_rec_, is_ati_rec_, is_pose_now_rec_,
  is_pose_des_rec_, is_cont_hist_rec_;
  
  void send_plot();

  ros::Time the_time;
  
  image_transport::ImageTransport it_viz;
  image_transport::Subscriber  img_now;
  void callback_img_now(const sensor_msgs::ImageConstPtr& msg);
  std::vector<float> img;
bool is_img_rec;
bool is_img_sent;
};

PlotMatlab::PlotMatlab():it_viz(nh)
{
    is_in_contact_ur_= 0;
   pixels_in_contact_ur_= 0;
   highest_force_cell_ur_= 0;
   real_total_force_ur_ = 0;
   
  float highest_force_cell_ur_;
  float real_total_force_ur_;
  
              is_recieved_new_joint_state_ = false;

   sub_joint_states = nh.subscribe("/joint_states", 1000, &PlotMatlab::jointStateCallback, this);

      current_state_sub = nh.subscribe("/current_state", 10, &PlotMatlab::cb_current_state_sub, this);
   current_state_ = 0;
  
  
  force = nh.subscribe("/wrench", 10, &PlotMatlab::callback_force, this);
  
  sub_feats_fb = nh.subscribe("/fb_feats", 10,  &PlotMatlab::callback_fbfeats,this);
  

//   pose_des = nh.subscribe("/pose_desired", 10,  &PlotMatlab::callback_posedes,this);
  
  contacts_hist = nh.subscribe("/contact_points_weiss_plot", 10,  &PlotMatlab::callback_contact_hist,this);
  contacts_hist1 = nh.subscribe("/contact_points_weiss_plot1", 10,  &PlotMatlab::callback_contact_hist1,this);
  contacts_hist2 = nh.subscribe("/contact_points_weiss_plot2", 10,  &PlotMatlab::callback_contact_hist2,this);
  contacts_hist3 = nh.subscribe("/contact_points_weiss_plot3", 10,  &PlotMatlab::callback_contact_hist3,this);
  contacts_hist4 = nh.subscribe("/contact_points_weiss_plot4", 10,  &PlotMatlab::callback_contact_hist4,this);
  contacts_hist5 = nh.subscribe("/contact_points_weiss_plot5", 10,  &PlotMatlab::callback_contact_hist5,this);
  contacts_hist6 = nh.subscribe("/contact_points_weiss_plot6", 10,  &PlotMatlab::callback_contact_hist6,this);
  contacts_hist7 = nh.subscribe("/contact_points_weiss_plot7", 10,  &PlotMatlab::callback_contact_hist7,this);
  contacts_hist8 = nh.subscribe("/contact_points_weiss_plot8", 10,  &PlotMatlab::callback_contact_hist8,this);
  contacts_hist9 = nh.subscribe("/contact_points_weiss_plot9", 10,  &PlotMatlab::callback_contact_hist9,this);
  contacts_hist10 = nh.subscribe("/contact_points_weiss_plot10", 10,  &PlotMatlab::callback_contact_hist10,this);
  contacts_hist11 = nh.subscribe("/contact_points_weiss_plot11", 10,  &PlotMatlab::callback_contact_hist11,this);
   contacts_hist12 = nh.subscribe("/contact_points_weiss_plot12", 10,  &PlotMatlab::callback_contact_hist12,this);
  contacts_hist13 = nh.subscribe("/contact_points_weiss_plot13", 10,  &PlotMatlab::callback_contact_hist13,this);
  contacts_hist14 = nh.subscribe("/contact_points_weiss_plot14", 10,  &PlotMatlab::callback_contact_hist14,this);
  contacts_hist15 = nh.subscribe("/contact_points_weiss_plot15", 10,  &PlotMatlab::callback_contact_hist15,this);
  
  contactsNO_hist = nh.subscribe("/contactNO_points_weiss_plot", 10,  &PlotMatlab::callback_contactNO_hist,this);
  contactsNO_hist1 = nh.subscribe("/contactNO_points_weiss_plot1", 10,  &PlotMatlab::callback_contactNO_hist1,this);
  contactsNO_hist2 = nh.subscribe("/contactNO_points_weiss_plot2", 10,  &PlotMatlab::callback_contactNO_hist2,this);
  contactsNO_hist3 = nh.subscribe("/contactNO_points_weiss_plot3", 10,  &PlotMatlab::callback_contactNO_hist3,this);
  contactsNO_hist4 = nh.subscribe("/contactNO_points_weiss_plot4", 10,  &PlotMatlab::callback_contactNO_hist4,this);
  contactsNO_hist5 = nh.subscribe("/contactNO_points_weiss_plot5", 10,  &PlotMatlab::callback_contactNO_hist5,this);
  contactsNO_hist6 = nh.subscribe("/contactNO_points_weiss_plot6", 10,  &PlotMatlab::callback_contactNO_hist6,this);
  contactsNO_hist7 = nh.subscribe("/contactNO_points_weiss_plot7", 10,  &PlotMatlab::callback_contactNO_hist7,this);
  contactsNO_hist8 = nh.subscribe("/contactNO_points_weiss_plot8", 10,  &PlotMatlab::callback_contactNO_hist8,this);
  contactsNO_hist9 = nh.subscribe("/contactNO_points_weiss_plot9", 10,  &PlotMatlab::callback_contactNO_hist9,this);
  contactsNO_hist10 = nh.subscribe("/contactNO_points_weiss_plot10", 10,  &PlotMatlab::callback_contactNO_hist10,this);
  contactsNO_hist11 = nh.subscribe("/contactNO_points_weiss_plot11", 10,  &PlotMatlab::callback_contactNO_hist11,this);
   contactsNO_hist12 = nh.subscribe("/contactNO_points_weiss_plot12", 10,  &PlotMatlab::callback_contactNO_hist12,this);
  contactsNO_hist13 = nh.subscribe("/contactNO_points_weiss_plot13", 10,  &PlotMatlab::callback_contactNO_hist13,this);
  contactsNO_hist14 = nh.subscribe("/contactNO_points_weiss_plot14", 10,  &PlotMatlab::callback_contactNO_hist14,this);
  contactsNO_hist15 = nh.subscribe("/contactNO_points_weiss_plot15", 10,  &PlotMatlab::callback_contactNO_hist15,this);

  pub_plot_matlab = nh.advertise<tactile_servo_msgs::PlotMatlabImg>("/plot_matlab", 10);
  
  is_feat_fb_rec_ = false;
  is_feat_des_rec_ = false;
  is_feat_plan_rec_ = false;
  is_ati_rec_ = false;
  is_pose_now_rec_ = false;
  is_pose_des_rec_ = false;
  is_cont_hist_rec_ =false;
  init_time = false;
  
  n_cycle = 0;
  
  img_now = it_viz.subscribe("/ros_tactile_image", 1, &PlotMatlab::callback_img_now, this);
  is_img_rec = 0;
  is_img_sent = 0;
}
PlotMatlab::~PlotMatlab()
{}



 void PlotMatlab::jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
 joint_values_.clear();  
joint_efforts_.clear();  
  
    if(msg->velocity.size() == 6)
    {
        for(int i = 0; i < msg->velocity.size(); i++)
        {
//     	    ROS_INFO("%d joint_name: %s", i, msg->name[i].c_str());
// 	    ROS_INFO("actual velocities: %lf", msg->velocity[i]);
            joint_values_.push_back(msg->position[i]);
	    joint_efforts_.push_back(msg->effort[i]);
// // 	    ROS_INFO("actual positions: %lf", msg->position[i]);	    
        }
            is_recieved_new_joint_state_ = true;

    }
}




void PlotMatlab::cb_current_state_sub(const std_msgs::Float64& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  current_state_ = msg.data;

  
}
   
void PlotMatlab::callback_img_now(const sensor_msgs::ImageConstPtr& msg)
{

   
  int rows_ = 4;
  int cols_ = 4;
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  
  if (is_img_sent == 1)
  {  
  int c = 0;
  for(int i=0; i<rows_; ++i) {
    for(int j=0; j<cols_; j++) {
      // make it visible
      img.push_back(cv_ptr->image.data[c]);
      c = c + 1;
    }
  }
  is_img_rec = 1;
  is_img_sent = 0;
  }
}
  
  
  
void PlotMatlab::callback_fbfeats(const tactile_servo_msgs::ContsFeats& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg.control_features.size() == 1)
  {
        is_in_contact_ur_ = msg.control_features[0].is_contact;
        pixels_in_contact_ur_ = msg.control_features[0].num_pixels_contact;
        highest_force_cell_ur_ = msg.control_features[0].highest_force_cell;
        real_total_force_ur_ = msg.control_features[0].real_total_force;
    fx_weiss = msg.control_features[0].contactForce;
    mx_weiss = msg.control_features[0].zmp_x;
    my_weiss = msg.control_features[0].zmp_y;
    copx_weiss =msg.control_features[0].centerpressure_x;
    copy_weiss=msg.control_features[0].centerpressure_y;
    orient_weiss=msg.control_features[0].contactOrientation;
    cocx_weiss=msg.control_features[0].centerContact_x;
    cocy_weiss=msg.control_features[0].centerContact_y;
    is_feat_fb_rec_ = true;
  }
  else
  {
    is_feat_fb_rec_ = false;
    ROS_DEBUG_STREAM("fb feat is not received");
  }
}



void PlotMatlab::callback_force(const geometry_msgs::WrenchStamped& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  fx = msg.wrench.force.x;
  fy = msg.wrench.force.y;
  fz = msg.wrench.force.z;
  mx = msg.wrench.torque.x;
  my = msg.wrench.torque.y;
  mz = msg.wrench.torque.z;
  
  is_ati_rec_ = true;
}



void PlotMatlab::callback_contact_hist(const geometry_msgs::PointStamped& msg)
{
  xc = msg.point.x;
  yc = msg.point.y;
  zc = msg.point.z;
}
void PlotMatlab::callback_contact_hist1(const geometry_msgs::PointStamped& msg)
{
  xc1 = msg.point.x;
  yc1 = msg.point.y;
  zc1 = msg.point.z;
}
void PlotMatlab::callback_contact_hist2(const geometry_msgs::PointStamped& msg)
{
  xc2 = msg.point.x;
  yc2 = msg.point.y;
  zc2 = msg.point.z;
}
void PlotMatlab::callback_contact_hist3(const geometry_msgs::PointStamped& msg)
{
  xc3 = msg.point.x;
  yc3 = msg.point.y;
  zc3 = msg.point.z;
}
void PlotMatlab::callback_contact_hist4(const geometry_msgs::PointStamped& msg)
{
  xc4 = msg.point.x;
  yc4 = msg.point.y;
  zc4 = msg.point.z;
}
void PlotMatlab::callback_contact_hist5(const geometry_msgs::PointStamped& msg)
{
  xc5 = msg.point.x;
  yc5 = msg.point.y;
  zc5= msg.point.z;
}
void PlotMatlab::callback_contact_hist6(const geometry_msgs::PointStamped& msg)
{
  xc6 = msg.point.x;
  yc6 = msg.point.y;
  zc6 = msg.point.z;
}
void PlotMatlab::callback_contact_hist7(const geometry_msgs::PointStamped& msg)
{
  xc7 = msg.point.x;
  yc7 = msg.point.y;
  zc7 = msg.point.z;
}

void PlotMatlab::callback_contact_hist8(const geometry_msgs::PointStamped& msg)
{
  xc8 = msg.point.x;
  yc8 = msg.point.y;
  zc8 = msg.point.z;
}
void PlotMatlab::callback_contact_hist9(const geometry_msgs::PointStamped& msg)
{
  xc9 = msg.point.x;
  yc9 = msg.point.y;
  zc9 = msg.point.z;
}
void PlotMatlab::callback_contact_hist10(const geometry_msgs::PointStamped& msg)
{
  xc10 = msg.point.x;
  yc10 = msg.point.y;
  zc10 = msg.point.z;
}
void PlotMatlab::callback_contact_hist11(const geometry_msgs::PointStamped& msg)
{
  xc11 = msg.point.x;
  yc11 = msg.point.y;
  zc11 = msg.point.z;
}
void PlotMatlab::callback_contact_hist12(const geometry_msgs::PointStamped& msg)
{
  xc12 = msg.point.x;
  yc12 = msg.point.y;
  zc12 = msg.point.z;
}
void PlotMatlab::callback_contact_hist13(const geometry_msgs::PointStamped& msg)
{
  xc13 = msg.point.x;
  yc13 = msg.point.y;
  zc13 = msg.point.z;
}
void PlotMatlab::callback_contact_hist14(const geometry_msgs::PointStamped& msg)
{
  xc14 = msg.point.x;
  yc14 = msg.point.y;
  zc14 = msg.point.z;
}
void PlotMatlab::callback_contact_hist15(const geometry_msgs::PointStamped& msg)
{
  xc15 = msg.point.x;
  yc15 = msg.point.y;
  zc15= msg.point.z;
}

//NOc

void PlotMatlab::callback_contactNO_hist(const geometry_msgs::PointStamped& msg)
{
  xNOc = msg.point.x;
  yNOc = msg.point.y;
  zNOc = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist1(const geometry_msgs::PointStamped& msg)
{
  xNOc1 = msg.point.x;
  yNOc1 = msg.point.y;
  zNOc1 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist2(const geometry_msgs::PointStamped& msg)
{
  xNOc2 = msg.point.x;
  yNOc2 = msg.point.y;
  zNOc2 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist3(const geometry_msgs::PointStamped& msg)
{
  xNOc3 = msg.point.x;
  yNOc3 = msg.point.y;
  zNOc3 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist4(const geometry_msgs::PointStamped& msg)
{
  xNOc4 = msg.point.x;
  yNOc4 = msg.point.y;
  zNOc4 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist5(const geometry_msgs::PointStamped& msg)
{
  xNOc5 = msg.point.x;
  yNOc5 = msg.point.y;
  zNOc5= msg.point.z;
}
void PlotMatlab::callback_contactNO_hist6(const geometry_msgs::PointStamped& msg)
{
  xNOc6 = msg.point.x;
  yNOc6 = msg.point.y;
  zNOc6 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist7(const geometry_msgs::PointStamped& msg)
{
  xNOc7 = msg.point.x;
  yNOc7 = msg.point.y;
  zNOc7 = msg.point.z;
}

void PlotMatlab::callback_contactNO_hist8(const geometry_msgs::PointStamped& msg)
{
  xNOc8 = msg.point.x;
  yNOc8 = msg.point.y;
  zNOc8 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist9(const geometry_msgs::PointStamped& msg)
{
  xNOc9 = msg.point.x;
  yNOc9 = msg.point.y;
  zNOc9 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist10(const geometry_msgs::PointStamped& msg)
{
  xNOc10 = msg.point.x;
  yNOc10 = msg.point.y;
  zNOc10 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist11(const geometry_msgs::PointStamped& msg)
{
  xNOc11 = msg.point.x;
  yNOc11 = msg.point.y;
  zNOc11 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist12(const geometry_msgs::PointStamped& msg)
{
  xNOc12 = msg.point.x;
  yNOc12 = msg.point.y;
  zNOc12 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist13(const geometry_msgs::PointStamped& msg)
{
  xNOc13 = msg.point.x;
  yNOc13 = msg.point.y;
  zNOc13 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist14(const geometry_msgs::PointStamped& msg)
{
  xNOc14 = msg.point.x;
  yNOc14 = msg.point.y;
  zNOc14 = msg.point.z;
}
void PlotMatlab::callback_contactNO_hist15(const geometry_msgs::PointStamped& msg)
{
  xNOc15 = msg.point.x;
  yNOc15 = msg.point.y;
  zNOc15= msg.point.z;
}

void PlotMatlab::send_plot()
{
   if ( is_feat_fb_rec_  )
  {
    tactile_servo_msgs::PlotMatlabImg plot_matl;
    plot_matl.header.frame_id = "1";
    plot_matl.header.stamp = ros::Time::now();
    
    plot_matl.fx = fx;
    plot_matl.fy = fy;
    plot_matl.fz = fz;
    plot_matl.fwx = mx;
    plot_matl.fwy = my;
    plot_matl.fwz = mz;
    
    plot_matl.coc_x = cocx_weiss;
    plot_matl.coc_y = cocy_weiss;
    plot_matl.f = fx_weiss;
    plot_matl.zmp_x = mx_weiss;
    plot_matl.zmp_y = my_weiss;
    plot_matl.orient_z = orient_weiss;
    plot_matl.cop_x = copx_weiss;
    plot_matl.cop_y = copy_weiss;
    
    plot_matl.coc_xd = cocx_weissd;
    plot_matl.coc_yd = cocy_weissd;
    plot_matl.fd = fx_weissd;
    plot_matl.zmp_xd = mx_weissd;
    plot_matl.zmp_yd = my_weissd;
    plot_matl.orient_zd = orient_weissd;
    plot_matl.cop_xd = copx_weissd;
    plot_matl.cop_yd = copy_weissd;
    
    plot_matl.num_contours = num_contours;
    
    plot_matl.x_now = x;
    plot_matl.y_now = y;
    plot_matl.z_now = z;
    plot_matl.wx_now = wx;
    plot_matl.wy_now = wy;
    plot_matl.wz_now = wz;
    plot_matl.ww_now = ww;
    
    plot_matl.x_des = xd;
    plot_matl.y_des = yd;
    plot_matl.z_des = zd;
    plot_matl.wx_des = wxd;
    plot_matl.wy_des = wyd;
    plot_matl.wz_des = wzd;
    plot_matl.ww_des = wwd;
    
    plot_matl.x_c = xc;
    plot_matl.y_c = yc;
    plot_matl.z_c = zc;

    plot_matl.x_c1 = xc1;
    plot_matl.y_c1 = yc1;
    plot_matl.z_c1 = zc1;

    plot_matl.x_c2 = xc2;
    plot_matl.y_c2 = yc2;
    plot_matl.z_c2 = zc2;

    plot_matl.x_c3 = xc3;
    plot_matl.y_c3 = yc3;
    plot_matl.z_c3 = zc3;

    plot_matl.x_c4 = xc4;
    plot_matl.y_c4 = yc4;
    plot_matl.z_c4 = zc4;

    plot_matl.x_c5 = xc5;
    plot_matl.y_c5 = yc5;
    plot_matl.z_c5 = zc5;

    plot_matl.x_c6 = xc6;
    plot_matl.y_c6 = yc6;
    plot_matl.z_c6 = zc6;

    plot_matl.x_c7 = xc7;
    plot_matl.y_c7 = yc7;
    plot_matl.z_c7 = zc7;

    plot_matl.x_c8 = xc8;
    plot_matl.y_c8 = yc8;
    plot_matl.z_c8 = zc8;

    plot_matl.x_c9 = xc9;
    plot_matl.y_c9 = yc9;
    plot_matl.z_c9 = zc9;

    plot_matl.x_c10 = xc10;
    plot_matl.y_c10 = yc10;
    plot_matl.z_c10 = zc10;    
    plot_matl.x_c11 = xc11;
    plot_matl.y_c11 = yc11;
    plot_matl.z_c11 = zc11;

    plot_matl.x_c12 = xc12;
    plot_matl.y_c12 = yc12;
    plot_matl.z_c12 = zc12;

    plot_matl.x_c13 = xc13;
    plot_matl.y_c13 = yc13;
    plot_matl.z_c13 = zc13;

    plot_matl.x_c14 = xc14;
    plot_matl.y_c14 = yc14;
    plot_matl.z_c14 = zc14;

    plot_matl.x_c15 = xc15;
    plot_matl.y_c15 = yc15;
    plot_matl.z_c15 = zc15;   
    
    
    
    //NOc
      plot_matl.xNO_c = xNOc;
    plot_matl.yNO_c = yNOc;
    plot_matl.zNO_c = zNOc;

    plot_matl.xNO_c1 = xNOc1;
    plot_matl.yNO_c1 = yNOc1;
    plot_matl.zNO_c1 = zNOc1;

    plot_matl.xNO_c2 = xNOc2;
    plot_matl.yNO_c2 = yNOc2;
    plot_matl.zNO_c2 = zNOc2;

    plot_matl.xNO_c3 = xNOc3;
    plot_matl.yNO_c3 = yNOc3;
    plot_matl.zNO_c3 = zNOc3;

    plot_matl.xNO_c4 = xNOc4;
    plot_matl.yNO_c4 = yNOc4;
    plot_matl.zNO_c4 = zNOc4;

    plot_matl.xNO_c5 = xNOc5;
    plot_matl.yNO_c5 = yNOc5;
    plot_matl.zNO_c5 = zNOc5;

    plot_matl.xNO_c6 = xNOc6;
    plot_matl.yNO_c6 = yNOc6;
    plot_matl.zNO_c6 = zNOc6;

    plot_matl.xNO_c7 = xNOc7;
    plot_matl.yNO_c7 = yNOc7;
    plot_matl.zNO_c7 = zNOc7;

    plot_matl.xNO_c8 = xNOc8;
    plot_matl.yNO_c8 = yNOc8;
    plot_matl.zNO_c8 = zNOc8;

    plot_matl.xNO_c9 = xNOc9;
    plot_matl.yNO_c9 = yNOc9;
    plot_matl.zNO_c9 = zNOc9;

    plot_matl.xNO_c10 = xNOc10;
    plot_matl.yNO_c10 = yNOc10;
    plot_matl.zNO_c10 = zNOc10;    
    plot_matl.xNO_c11 = xNOc11;
    plot_matl.yNO_c11 = yNOc11;
    plot_matl.zNO_c11 = zNOc11;

    plot_matl.xNO_c12 = xNOc12;
    plot_matl.yNO_c12 = yNOc12;
    plot_matl.zNO_c12 = zNOc12;

    plot_matl.xNO_c13 = xNOc13;
    plot_matl.yNO_c13 = yNOc13;
    plot_matl.zNO_c13 = zNOc13;

    plot_matl.xNO_c14 = xNOc14;
    plot_matl.yNO_c14 = yNOc14;
    plot_matl.zNO_c14 = zNOc14;

    plot_matl.xNO_c15 = xNOc15;
    plot_matl.yNO_c15 = yNOc15;
    plot_matl.zNO_c15 = zNOc15; 
    plot_matl.is_in_contact_ur = is_in_contact_ur_;
    plot_matl.pixels_in_contact_ur = pixels_in_contact_ur_;
    plot_matl.highest_force_cell_ur = highest_force_cell_ur_;
    plot_matl.real_total_force_ur = real_total_force_ur_;

    plot_matl.current_state_ur = current_state_;
    
    if (is_recieved_new_joint_state_)
    {
      for (int ind = 0; ind < joint_values_.size(); ind++)
      {
	plot_matl.joint_angles.push_back(joint_values_.at(ind));
	plot_matl.joint_efforts.push_back(joint_efforts_.at(ind));

      }
      is_recieved_new_joint_state_ = 0;
      joint_values_.clear();
      joint_efforts_.clear();
    }
   
    
    if (is_img_rec)
    {
      for (int ind = 0; ind < img.size(); ind++)
      {
	plot_matl.img.push_back(img.at(ind));
      }
      is_img_rec = 0;
      img.clear();
    }
    
    if (!init_time)
    {
      test_time_begin = ros::Time::now();
    }
    // correct time
    test_time = ros::Time::now() - test_time_begin;
    
    if(init_time)
    {
      dt = ros::Time::now() - t_old_sec_total;
    }
    
    t_old_sec_total = ros::Time::now();
    
    if(init_time)
    {
      n_cycle = n_cycle + 1;
      plot_matl.dt = dt.toSec();
      test_time2 = dt.toSec() * n_cycle;
      // not correct time as the dt is not stable
      plot_matl.test_time2 = test_time2;
    }
    
    init_time = true;
    
    plot_matl.time = test_time.toSec();
    
    ROS_DEBUG_STREAM("all_recieved");
    pub_plot_matlab.publish(plot_matl);
    is_feat_des_rec_ = false;
    is_feat_fb_rec_ = false;
    is_pose_now_rec_ = false;
    is_pose_des_rec_ = false;
    
    img.clear();
    plot_matl.img.clear();
    plot_matl.joint_angles.clear();
    plot_matl.joint_efforts.clear();

    is_img_sent = 1;
  }
  
}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "plot_matlab_image");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    ROS_INFO("plot_matlab_image");
    PlotMatlab plot_matlab;
    while( ros::ok() )
    {
      plot_matlab.send_plot();
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


