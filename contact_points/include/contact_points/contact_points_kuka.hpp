/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   Thu Mar 10 11:07:10 2011
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for tactile image processing
 */
#ifndef _CONTACT_POINTS_KUKA_HPP_
#define _CONTACT_POINTS_KUKA_HPP_

#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "tactile_servo_msgs/Image_weiss.h"


#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <geometry_msgs/PointStamped.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudColor;


class ContactPointsKuks
{
  
public:
  ContactPointsKuks(int sensors_number);
  ~ContactPointsKuks();
  
//   void publish_contacts();

  void img2points(cv::Mat& input_img);
  void transformPoint(const tf::TransformListener& listener_, 
		      geometry_msgs::PointStamped& point_in_, 
		      geometry_msgs::PointStamped& pose_out );
   
  tf::TransformListener tf_listener;
  ros::Publisher pub_contacts;
  ros::Publisher pub_contacts_color;
  ros::Publisher pub_contact_positions_plot;


protected:
  // can be inhereted and used by derived class
  ros::NodeHandle nh;
  void get_sensor_params(int& tactile_sensor_num);
  void ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist,
			  int& tactile_sensor_num);  

//   void image_cb_viz(const sensor_msgs::ImageConstPtr& msg);

  int rows_, cols_, step_;
  double size_x_, size_y_, tactel_area_;
  
  std::string tact_img_top_name_, tact_img_top_name_12bit_;
  std::string fb_feats_control_top_name_, fb_feats_plan_top_name_, 
  des_feats_top_name_;
  
  int noise_threshold_, coc_threshold_, area_threshold_,
  contact_points_n_;
  double pres_from_image_, force_scale_;
  
  std::string sensor_frame_name_urdf_ ;

  int scaling_factor_;
  
  float total_pressure_, contact_orientation_;
  
  std::string contact_points_topic_name_;
  
  double hight_of_sensor_;
  
  int threshold_noise_;
  
  double compliance_ ;
  
  std::string base_frame_name_urdf_;
  
  std::string  contact_points_topic_name_to_record_;
private:
  
  image_transport::ImageTransport it;
  image_transport::Subscriber image_sub;
  void image_cb(const sensor_msgs::ImageConstPtr& msg);
    std::string contact_points_topic_name_1;
  std::string contact_points_topic_name_2;
  std::string contact_points_topic_name_3;
  std::string contact_points_topic_name_4;
  std::string contact_points_topic_name_5;
  std::string contact_points_topic_name_6;
  std::string contact_points_topic_name_7;
  std::string contact_points_topic_name_8;
  std::string contact_points_topic_name_9;
  std::string contact_points_topic_name_10;
  std::string contact_points_topic_name_11;
  std::string contact_points_topic_name_12;
  std::string contact_points_topic_name_13;
  std::string contact_points_topic_name_14;
  std::string contact_points_topic_name_15;

  
};


#endif