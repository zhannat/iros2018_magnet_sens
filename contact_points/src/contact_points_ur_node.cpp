/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include "contact_points/contact_points_ur.hpp"
#include <ros/ros.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "contact_points_ur");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    ROS_INFO("contact_points_node");
    ContactPointsKuks point_cloud_weiss(0);
    
    
    while( ros::ok() )
    {
      
      ros::spinOnce();
      loop_rate.sleep(); 
    }
    
}


