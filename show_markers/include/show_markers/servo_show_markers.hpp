/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @brief  This is a library for a set of PIDs for servoing. 
*/
#ifndef _SERVO_SHOW_MARKERS_HPP_
#define _SERVO_SHOW_MARKERS_HPP_

#include <ros/ros.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/PlanFeats.h"

#include <visualization_msgs/Marker.h>

// for transformations
#include <tf/tf.h>
#include <tf/transform_listener.h>

//frame broadcaster
#include <tf/transform_broadcaster.h>

class markersShow
{
public:
    markersShow (int tactile_sensor_num_);
    ~markersShow();
    ros::NodeHandle n;

    ros::Subscriber des_feats_sub;
    ros::Subscriber fb_feats_sub;
    ros::Subscriber fb_planfeats_sub;
    void cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    void cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    void cb_fb_planfeats_sub(const tactile_servo_msgs::PlanFeats& msg_);
    int contours_num;
    
    ros::Publisher cop_des_pub;
    ros::Publisher cop_fb_pub;
    ros::Publisher forcez_des_pub;
    ros::Publisher forcez_fb_pub;
    ros::Publisher coc_des_pub;
    ros::Publisher coc_fb_pub;
    ros::Publisher orientz_des_pub;
    ros::Publisher orientz_fb_pub;
    ros::Publisher forcez_diff_fb_des_pub;

    geometry_msgs::PointStamped transform_point(
            geometry_msgs::PointStamped& point_in_);
    geometry_msgs::PoseStamped transform_pose(
            geometry_msgs::PoseStamped& pose_in_);

    visualization_msgs::Marker create_marker(geometry_msgs::PoseStamped& point_to_viz_,
                                             std::string frame_name_,
                                            std::vector<float> color_,
                                             u_int32_t shape_,
                                             std::vector<float> scale_);
    geometry_msgs::PoseStamped fillup_pose(float x_, float y_, float z_, float qx_,
                                                        float qy_,float qz_,float qw_);

    void rviz();
    tf::TransformListener tf_listener;

    // sensor parameters
    int cells_x;
    int cells_y;
    double size_x;
    double size_y;

    //coordinates
    float copx_fb,
    copy_fb,
    force_fb,
    cocx_fb,
    cocy_fb,
    orientz_fb;
    float copx_des,
    copy_des,
    force_des,
    cocx_des,
    cocy_des,
    orientz_des;
    // visual params
    float thickness_z,
    forcr_vector_length;

    std::vector<float> color_cop_des,
    color_cop_fb ,
    color_coc_des,
    color_coc_fb,
    color_forcez_des,
    color_forcez_fb ,
    color_orientz_des,
    color_orientz_fb,
    color_forcez_diff;
    std::vector<float> scale_default,
    scale_arrow,
    scale_cylinder;

};

class PoseShow
{
public:
  PoseShow();
  ~PoseShow();
  
  ros::Subscriber pose_viz_des_sub;
  ros::Subscriber pose_viz_des2_sub;
  ros::Subscriber pose_viz_now_sub;
  ros::Subscriber pose_viz_now2_sub;
  ros::Subscriber pose_viz_contact_frame_sub;
  ros::Subscriber pose_viz_line_follow_next_sub;

  
  /** TODO instead of declearing each Subscriber and broadcaster
   * we can create a vector of Subscribers, vecotr of transform_
   * vector of broadcasters and use one callback with boos::bind 
   * which will allow to send more arguments to callback function
   * */
  
  std::vector<ros::Subscriber> pose_viz;
  std::vector<tf::TransformBroadcaster> broadcast_frame;

  void cb_pose_viz_des_sub(const geometry_msgs::PoseStampedPtr& msg);
  void cb_pose_viz_des2_sub(const geometry_msgs::PoseStampedPtr& msg);
  void cb_pose_viz_now_sub(const geometry_msgs::PoseStampedPtr& msg);
  void cb_pose_viz_now2_sub(const geometry_msgs::PoseStampedPtr& msg);
  void cb_pose_viz_contact_frame_sub(const geometry_msgs::PoseStampedPtr& msg);
  void cb_pose_viz_line_follow_next_sub(const geometry_msgs::PoseStampedPtr& msg);

  tf::Transform transform_;
  tf::TransformBroadcaster br_frame_;
  
  tf::Transform transform_des2_;
  tf::TransformBroadcaster br_frame_des2_;
  
  tf::Transform transform_now_;
  tf::TransformBroadcaster br_frame_now_;
  
  tf::Transform transform_now2_;
  tf::TransformBroadcaster br_frame_now2_;
  
  tf::Transform transform_contact_;
  tf::TransformBroadcaster br_frame_contact_;
  
  tf::Transform transform_line_follow_;
  tf::TransformBroadcaster br_line_follow_next_;
  
  void br();
  void br_des2();
  void br_now();
  void br_now2();
  void br_contact();
  void br_line_follow();

  
private:
  ros::NodeHandle nh;

};

#endif