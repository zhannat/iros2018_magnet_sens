/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   Thu Mar 10 11:07:10 2011
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for tactile image processing
 */
#ifndef _TACTILE_IMG_PROC_KUKA_HPP_
#define _TACTILE_IMG_PROC_KUKA_HPP_

#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <dynamic_reconfigure/server.h>

#include <tactile_servo_config/img_proc_paramsConfig.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/Image_weiss.h"
#include "tactile_servo_msgs/PlanFeats.h"

#include "tactile_servo_msgs/compare_eigens.h"

#include <std_msgs/Bool.h>

#include <std_msgs/Float64.h>

#include "tactile_servo_msgs/COCtoZMP.h"
class ImageToolsKuka
{
  
public:
  ImageToolsKuka(int sensors_number);
  ~ImageToolsKuka();
  
  bool is_in_contact_ur_;
  int pixels_in_contact_ur_;
  float highest_force_cell_ur_;
  float real_total_force_ur_;

  
  void publish_features();
  std::vector<cv::Point2d> eigen_vecs_help_;
  std::vector<double> eigen_val_help_;
  int isContact;
  ros::Publisher start_tactile;
  std_msgs::Bool switch_from_traj_to_tactile;
  
  // topic force proportional to contact area

  ros::Publisher force_estim_pub;
  std_msgs::Float64 estim_force_area;
  std_msgs::Float64 estim_force_area_old_;
float force_res;
protected:
  // can be inhereted and used by derived class
  ros::NodeHandle nh;
  void get_sensor_params(int& tactile_sensor_num);
  void ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist,
			  int& tactile_sensor_num);  

//   void image_cb_viz(const sensor_msgs::ImageConstPtr& msg);

  int rows_, cols_, step_;
  double size_x_, size_y_, tactel_area_;
  
  std::string tact_img_top_name_, tact_img_top_name_12bit_;
  std::string fb_feats_control_top_name_, fb_feats_plan_top_name_, 
  des_feats_top_name_;
  
  int noise_threshold_, coc_threshold_, area_threshold_,
  contact_points_n_;
  double pres_from_image_, force_scale_;
  double torque_from_image_x_, torque_from_image_y_;
  
  int scaling_factor_;
  
  float total_pressure_, contact_orientation_;
  
  double pca_orientation_old_;
  int compare_eigens_;

  float cocy_, cocx_;
  float zmpx_, zmpy_;
  float force_;
  float force_old_;
  float contact_orientation_old_;
  double sample_factor_;
  double filter_length_;
  float low_pass(float& in_new, float& in_old);
  float zmpy_old_, zmpx_old_;
  float pca_orient_;
  int max_intensity_;
  float cocx_old_;
  float cocy_old_;
  float copx_;
  float copy_;
  float copx_old_;
  float copy_old_;
  float weighted_x_16bit_, weighted_y_16bit_;
  
  double force_from_area_;
  
  ros::Publisher num_pixels_weight_gain;
  tactile_servo_msgs::COCtoZMP coc2zmp;
  int coc2zmp_begin_num_pixels_, coc2zmp_end_num_pixels_;
  double coc2zmp_pixels2smoothtrans_;
  


private:
  
  image_transport::ImageTransport it;
  image_transport::Subscriber image_sub;
  void image_cb(const sensor_msgs::ImageConstPtr& msg);
  
//   image_transport::ImageTransport it_orient;
//   image_transport::Subscriber image_sub_orient;
//   void image_cb_orient(const sensor_msgs::ImageConstPtr& msg);
//   float orient_from_sensitive_img_;
  
  ros::Subscriber image_sub_force;
  void image_cb_force(const tactile_servo_msgs::Image_weiss& msg_weiss);
  
  //moments topic
  tactile_servo_msgs::ContsFeats fb_feat_set;
  tactile_servo_msgs::OneContFeats fb_one_cont_feats;
  ros::Publisher fb_feats_pub;
   

  // planning topics contours and pca
  tactile_servo_msgs::PlanFeats fb_feat_plan_set;
  ros::Publisher fb_plan_pub;
  
  //PCA topic
  tactile_servo_msgs::ContsFeats fb_feat_set_pca;
  tactile_servo_msgs::OneContFeats fb_one_cont_feats_pca;
  ros::Publisher fb_feats_pub_pca;


  void cb_dynamic_reconf(tactile_servo_config::img_proc_paramsConfig &config, uint32_t level);
  dynamic_reconfigure::Server<tactile_servo_config::img_proc_paramsConfig> change_improc_param_server;
  dynamic_reconfigure::Server<tactile_servo_config::img_proc_paramsConfig> :: CallbackType f;
  
  
 
  void img_proc_moments(cv::Mat& input_img);
  void img_proc_pca(cv::Mat& input_img);
  
  cv::Point2d cop, coc;
  std::string sensor_frame_name_urdf_ ;
  
  cv::Mat bin_img_;
  cv::Point2d coc_one_, coc_two_;
  double area_contact_one_, area_contact_two_;

  void apply_pca( std::vector<cv::Point> &pts, 
		  int contour_num, 
		  int tot_num_contours);
  
  void weighted_center(cv::Mat& input_img, float& weighted_x, float& weighted_y);


    
};

//publish eigen values

class ImageHelpToolsKuka
{
public:
   ImageHelpToolsKuka(ImageToolsKuka& img_tools_obj_ref );
  ~ImageHelpToolsKuka();
  void send_eigens();
//   void parallel_loop();

private:
  ros::NodeHandle nh_eigen;
  ImageToolsKuka& img_tools_obj_ref_;
  ros::Publisher pub_eigens;
};



/**

class VisFeats
{
public:
  VisFeats();
  ~VisFeats();
  
private:
  ros::NodeHandle nh;
  image_transport::ImageTransport it;
  image_transport::Subscriber image_sub;
  ros::Subscriber feats_des;

  
  void image_cb(const sensor_msgs::ImageConstPtr& msg);
  void cb_dynamic_reconf(tactile_servo_config::img_proc_paramsConfig
			  &config, uint32_t level);
  



};



  
  
  
  

**/

#endif