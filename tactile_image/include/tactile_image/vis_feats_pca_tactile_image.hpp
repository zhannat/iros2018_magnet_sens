#ifndef _VIS_FEATS_PCA_TACTILE_IMG_
#define _VIS_FEATS_PCA_TACTILE_IMG_

#include "tactile_image/tactile_image_processing.hpp"

// #include "tactile_image_processing.hpp"


class VisFeatsPCA : protected ImageTools
{
public:

  VisFeats(int sensors_number);
  ~VisFeats();
  void show_edge_feats();
  void show_point_feats();

private:
  
  ros::Subscriber des_feats_sub;
  ros::Subscriber fb_feats_sub;
  ros::Subscriber fb_plan_sub;
  ros::Subscriber fb_feats_pca_sub;

  void cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
  void cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
  
  void cb_plan_sub(const tactile_servo_msgs::PlanFeatsConstPtr& msg_);
  void cb_fb_feats_pca_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
  
  
  float copx_des,
  copy_des,
  force_des,
    cocx_des,
    cocy_des,
    orientz_des;
  float copx_fb,
  copy_fb,
  force_fb,
    cocx_fb,
    cocy_fb,
    orientz_fb;
  
  cv::Point2d cop_viz, coc_viz;
  cv::Point2d cop_viz_des, coc_viz_des;
 
  
  void sizes2pixels(float& x, float& y, cv::Point2d& ret_point);
  

  
  void draw_circle(cv::Mat& img, cv::Point2d& center,
		   int r, int g, int b);
  
  void draw_gunsight(cv::Mat& img, cv::Point2d& center,
		     int r, int g, int b);
  
  void draw_line(cv::Mat& img, cv::Point2d& center, 
		 float orient, int r, int g, int b);

  void draw_squares(cv::Mat& img, cv::Point2d& center,
		    int r, int g, int b);
  
  image_transport::ImageTransport it_viz;
  image_transport::Subscriber image_sub_viz;
  void image_cb_viz(const sensor_msgs::ImageConstPtr& msg);
  cv::Mat src_img_;
  
  void show_windows(cv::Mat& img_show_feats);


};
#endif