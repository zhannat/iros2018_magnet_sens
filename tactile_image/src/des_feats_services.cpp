/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to extract features 
 * requires parameters on ros param server /tactile_array....
 */

#include <ros/ros.h>


#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_srvs/set_des_feats_srv.h"

#include "tactile_servo_srvs/set_des_feats_srvRequest.h"

#include "tactile_servo_srvs/set_des_feats_srvResponse.h"
class plannerDesFeatsSrv
{
public:
    plannerDesFeatsSrv();
    ~plannerDesFeatsSrv();

    ros::NodeHandle nh;

    ros::Publisher pub_feats;
    ros::ServiceClient set_feats_client;
    tactile_servo_srvs::set_des_feats_srv des_feats_set;

   
    void pub();
    void pub2();
    float copx, copy, force, cocx, cocy, orientz, zmp_x, zmp_y;
    

};
plannerDesFeatsSrv::plannerDesFeatsSrv(){
   
  ros::service::waitForService("/set_des_feats",ros::Duration(1));
  ROS_INFO("connected service provider");
  
  set_feats_client = nh.serviceClient<tactile_servo_srvs::set_des_feats_srv>("/set_des_feats");
  
    copx = 0.0;
    copy = 0.0;
    force = 0.0;
    cocx = 0.0;
    cocy = 0.0;
    orientz = 0.0;
    zmp_x = 0.0;

}
plannerDesFeatsSrv::~plannerDesFeatsSrv(){}

void plannerDesFeatsSrv::pub()
{
  des_feats_set.request.cocx = 0;
  des_feats_set.request.cocy = 0;
  des_feats_set.request.force = 6.3;
  des_feats_set.request.zmp_x = -0.08;
  des_feats_set.request.zmp_y = 0.0;
  des_feats_set.request.orient = 0;
  if(set_feats_client.call(des_feats_set))
  {
//     ROS_INFO_STREAM ( " res =" << des_feats_set.response.success);
    
  }
  else
  {
    ROS_INFO_STREAM ("Failed to set");
   
  }
}

void plannerDesFeatsSrv::pub2()
{
  des_feats_set.request.cocx = 0.0;
  des_feats_set.request.cocy = 0.0;
  des_feats_set.request.force = 6.3;
  des_feats_set.request.zmp_x = 0.08;
  des_feats_set.request.zmp_y = 0;
  des_feats_set.request.orient = 0.00;
  if(set_feats_client.call(des_feats_set))
  {
//     ROS_INFO_STREAM ( " res =" << des_feats_set.response.success);
    
  }
  else
  {
    ROS_INFO_STREAM ("Failed to set");
   
  }
}



int main(int argc, char** argv)
{
    ros::init(argc, argv, "set_des_feats_service_squarewave");
    ros::NodeHandle n;

    ros::Rate loop_rate(100);
    plannerDesFeatsSrv practise1;
int bandwidth;
bandwidth = 500;
    int boollet_proof, interred_one, interred_two;;
    boollet_proof = 1;
    interred_two = 0;
    interred_one =0;
    int counter = 1;
    while( ros::ok() ){
//        std::cout<<"copx to pub = "<< practise1.copx<<std::endl;
//        std::cout<<"in loop "<< std::endl;
      if (counter < bandwidth)
      {
        practise1.pub();
        ros::spinOnce();
	interred_one = 1;
	interred_two = 0;
	
      }
      if (counter > bandwidth)
      {
        practise1.pub2();
        ros::spinOnce();
	interred_two = 1;
	interred_one = 0;
      }
      
      
//       if ((interred_one == 1)&&(interred_two == 0))
//       {
//       boollet_proof = 2;
//       }
//       if ((interred_one == 0)&&(interred_two == 1))
//       {
//       boollet_proof = 1;
//       }
      counter = counter + 1;
      if (counter > bandwidth*2) counter = 1;
      loop_rate.sleep();
    }
    return 0;
}  