/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to extract features 
 * requires parameters on ros param server /tactile_array....
 */

#include <ros/ros.h>


#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
class plannerDesFeats
{
public:
    plannerDesFeats();
    ~plannerDesFeats();

    ros::NodeHandle nh;

    ros::Publisher pub_feats;

    void pub();
    void pub2();
    float copx, copy, force, cocx, cocy, orientz, zmp_x, zmp_y;
    tactile_servo_msgs::ContsFeats des_feat_set;
    tactile_servo_msgs::OneContFeats one_cont_feats;


};
plannerDesFeats::plannerDesFeats(){
   
    pub_feats = nh.advertise<tactile_servo_msgs::ContsFeats>("des_feats",1);
    des_feat_set.header.frame_id = "smth";
    copx = 0.0;
    copy = 0.0;
    force = 0.0;
    cocx = 0.0;
    cocy = 0.0;
    orientz = 0.0;
    zmp_x = 0.0;
    zmp_y = 0.0;

}
plannerDesFeats::~plannerDesFeats(){}

void plannerDesFeats::pub(){
    des_feat_set.header.stamp = ros::Time::now();
    one_cont_feats.centerpressure_x = copx;
    one_cont_feats.centerpressure_y = copy;
    one_cont_feats.contactForce = 2;
    one_cont_feats.centerContact_x = cocx;
    one_cont_feats.centerContact_y = cocy;
    one_cont_feats.contactOrientation = orientz;
    one_cont_feats.zmp_x = zmp_x;
    one_cont_feats.zmp_y = zmp_y;
    des_feat_set.control_features.push_back(one_cont_feats);
    pub_feats.publish(des_feat_set);
    des_feat_set.control_features.clear();
}

void plannerDesFeats::pub2(){
    des_feat_set.header.stamp = ros::Time::now();
    one_cont_feats.centerpressure_x = copx;
    one_cont_feats.centerpressure_y = copy;
    one_cont_feats.contactForce = 4;
    one_cont_feats.centerContact_x = cocx;
    one_cont_feats.centerContact_y = cocy;
    one_cont_feats.contactOrientation = orientz;
    one_cont_feats.zmp_x = zmp_x;
    one_cont_feats.zmp_y = zmp_y;
    des_feat_set.control_features.push_back(one_cont_feats);
    pub_feats.publish(des_feat_set);
    des_feat_set.control_features.clear();
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "set_des_feats");
    ros::NodeHandle n;

    ros::Rate loop_rate(100);
    plannerDesFeats practise1;

    int boollet_proof, interred_one, interred_two;;
    boollet_proof = 1;
    interred_two = 0;
    interred_one =0;
    int counter = 1;
    while( ros::ok() ){
//        std::cout<<"copx to pub = "<< practise1.copx<<std::endl;
//        std::cout<<"in loop "<< std::endl;
      if (counter < 500)
      {
        practise1.pub();
        ros::spinOnce();
	interred_one = 1;
	interred_two = 0;
	
      }
      if (counter > 500)
      {
        practise1.pub2();
        ros::spinOnce();
	interred_two = 1;
	interred_one = 0;
      }
      
      
//       if ((interred_one == 1)&&(interred_two == 0))
//       {
//       boollet_proof = 2;
//       }
//       if ((interred_one == 0)&&(interred_two == 1))
//       {
//       boollet_proof = 1;
//       }
      counter = counter + 1;
      if (counter > 1000) counter = 1;
      loop_rate.sleep();
    }
    return 0;
}  