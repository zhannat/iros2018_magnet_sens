/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   Thu Mar 10 11:07:10 2011
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for tactile image processing
 */
 
#include "tactile_image/vis_feats_tactile_image.hpp"
// #include "tactile_image/tactile_image_processing.hpp"

VisFeats::VisFeats(int sensors_number) : ImageTools(sensors_number), it_viz(nh)
{
 
  
  get_sensor_params(sensors_number);
  image_sub_viz = it_viz.subscribe("/"+tact_img_top_name_, 1,
			   &VisFeats::image_cb_viz, this);
  
  des_feats_sub = nh.subscribe(des_feats_top_name_, 1,  &VisFeats::cb_des_feats_sub,this);
  fb_feats_sub = nh.subscribe(fb_feats_control_top_name_, 1,  &VisFeats::cb_fb_feats_sub,this);
  
  fb_feats_pca_sub = nh.subscribe("/fb_feats_pca", 1,  &VisFeats::cb_fb_feats_pca_sub,this);
  fb_plan_sub = nh.subscribe(fb_feats_plan_top_name_, 1,  &VisFeats::cb_plan_sub,this);
 
  
  //fb_feats_pca_sub = nh.subscribe("/fb_feats_pca", 1,  &VisFeats::cb_fb_feats_pca_sub,this);

  copx_des = 0;
  copy_des = 0;
  force_des =0;
  cocx_des=0;
  cocy_des =0;
  orientz_des=0;
  copx_fb=0;
  copy_fb=0;
  force_fb=0;
  cocx_fb=0;
  cocy_fb=0;
  orientz_fb=0;

  scaling_factor_ = 30;
  coc_threshold_ = 5;
  
  cop_viz.x = 0;
  cop_viz_des.x = 0;
  cop_viz.y = 0;
  cop_viz_des.y = 0;
  
  cv::namedWindow("I&F", cv::WINDOW_NORMAL);
  cv::namedWindow("b&w", cv::WINDOW_NORMAL);
  
  cv::namedWindow("I&F_pca", cv::WINDOW_NORMAL);
//   cv::namedWindow("not_filtered", WINDOW_NORMAL);
  cv::startWindowThread();
}

VisFeats::~VisFeats()
{
  cv::destroyAllWindows();
}

void VisFeats::show_point_feats()
{
  cv::Mat rsz_cop;
  cv::resize(src_img_, rsz_cop, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
  cv::Mat rsz_color_cop;
  cv::cvtColor(rsz_cop, rsz_color_cop, CV_GRAY2BGR);
  
  /////////////////
  //////  fb  /////
  /////////////////
  // cop //
  /////////
  draw_circle(rsz_color_cop, cop_viz, 255, 0, 0);
  //////////////////
  //////  des  /////
  //////////////////
  //cop
  draw_circle(rsz_color_cop, cop_viz_des, 0, 255, 0);
  show_windows(rsz_color_cop);
}

void VisFeats::show_edge_feats()
{
  cv::Mat rsz_cop;
  cv::resize(src_img_, rsz_cop, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
  cv::Mat rsz_color_cop;
  cv::cvtColor(rsz_cop, rsz_color_cop, CV_GRAY2BGR);
  
  /////////////////
  //////  fb  /////
  /////////////////
  /////////
  // cop //
  /////////
  draw_circle(rsz_color_cop, cop_viz, 255, 0, 0);
//   cv::circle(rsz_color_cop, scaling_factor_*cop_viz, 3, CV_RGB(255, 0, 0), 3);
  /////////
  // coc //
  /////////
  draw_gunsight(rsz_color_cop, coc_viz, 255, 0, 0);
  /*
  cv::line(rsz_color_cop, scaling_factor_*coc_viz, scaling_factor_*(coc_viz + 0.1) ,
	    CV_RGB(255, 0, 0), 3,8);
  cv::line(rsz_color_cop, scaling_factor_*coc_viz, scaling_factor_*(coc_viz - 0.1) ,
	    CV_RGB(255, 0, 0), 3,8);
  
  cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
	    cv::Point2d(scaling_factor_*(coc_viz.x - 0.1) ,scaling_factor_*(coc_viz.y + 0.1) ),
	    CV_RGB(255, 0, 0), 3,8);
  cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
	    cv::Point2d(scaling_factor_*(coc_viz.x + 0.1) ,scaling_factor_*(coc_viz.y - 0.1) ),
	    CV_RGB(255, 0, 0), 3,8);*/
  ////////////
  // orient //
  ////////////
  draw_line(rsz_color_cop, coc_viz, orientz_fb, 255, 0, 0);
/*
    float y_temp_val_to_show_orient = std::tan(orientz_fb);
    cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz + 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);
    cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz - 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);*/

  //////////////////
  //////  des  /////
  //////////////////
  //cop
  draw_circle(rsz_color_cop, cop_viz_des, 0, 255, 0);
//   cv::circle(rsz_color_cop, scaling_factor_*cop_viz_des, 3, CV_RGB(255, 0, 0), 3);
  //coc
  draw_gunsight(rsz_color_cop, coc_viz_des, 0, 255, 0);
 /*cv::line(rsz_color_cop, scaling_factor_*coc_viz_des, scaling_factor_*(coc_viz_des + 0.1) ,
	    CV_RGB(255, 0, 0), 3,8);
  cv::line(rsz_color_cop, scaling_factor_*coc_viz_des, scaling_factor_*(coc_viz_des - 0.1) ,
	    CV_RGB(255, 0, 0), 3,8);
  
  cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
	    cv::Point2d(scaling_factor_*(coc_viz.x - 0.1) ,scaling_factor_*(coc_viz.y + 0.1) ),
	    CV_RGB(255, 0, 0), 3,8);
  cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
	    cv::Point2d(scaling_factor_*(coc_viz.x + 0.1) ,scaling_factor_*(coc_viz.y - 0.1) ),
	    CV_RGB(255, 0, 0), 3,8);
 */ 
 // oreint
 draw_line(rsz_color_cop, coc_viz, orientz_fb, 0, 255, 0);
/*  float y_temp_val_to_show_orient = std::tan(orientz_fb);
//  cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz + 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);
//  cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz - 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8); */
 show_windows(rsz_color_cop);

}
void VisFeats::draw_circle(cv::Mat& img, cv::Point2d& center,
		   int r, int g, int b)
{
  cv::circle(img, scaling_factor_*center, 3, CV_RGB(r, g, b), 3);
}

void VisFeats::draw_gunsight(cv::Mat& img, cv::Point2d& center,
			     int r, int g, int b)
{
  cv::line(img, scaling_factor_*center, scaling_factor_* cv::Point2d(center.x + 0.1,center.y + 0.1) ,
	    CV_RGB(r, g, b), 3,8);
  cv::line(img, scaling_factor_*center, scaling_factor_*cv::Point2d(center.x - 0.1,center.y - 0.1) ,
	    CV_RGB(r, g, b), 3,8);
  
  cv::line(img, scaling_factor_*center, 
	    cv::Point2d(scaling_factor_*(center.x - 0.1) ,scaling_factor_*(center.y + 0.1) ),
	    CV_RGB(r, g, b), 3,8);
  cv::line(img, scaling_factor_*center, 
	    cv::Point2d(scaling_factor_*(center.x + 0.1) ,scaling_factor_*(center.y - 0.1) ),
	    CV_RGB(r, g, b), 3,8);
}

void VisFeats::draw_line(cv::Mat& img, cv::Point2d& center, float orient,
			 int r, int g, int b)
{
  float y_temp_val_to_show_orient = std::tan(orient);
  cv::line(img, scaling_factor_*center, 
	   (scaling_factor_*center +  
	   2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, 
			   scaling_factor_)) ,
	   CV_RGB(r, g, b), 3,8);
  cv::line(img, scaling_factor_*center, 
	   (scaling_factor_*center - 
	   2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, 
			   scaling_factor_)) , 
	   CV_RGB(r, g, b), 3,8);
}


void VisFeats::draw_squares(cv::Mat& img, cv::Point2d& center,
			    int r, int g, int b)
{
  /*
    
    //coc rect
//     cv::Rect rect1;
//     rect1.x = scaling_factor_*(coc_viz.x-0.5);
//     rect1.y = scaling_factor_*(coc_viz.y-0.5);
//     rect1.width = scaling_factor_*0.5;
//     rect1.height = scaling_factor_*0.5;
//     cv::Rect rect2;
//     rect2.x = scaling_factor_*(coc_viz.x);
//     rect2.y = scaling_factor_*coc_viz.y;
//     rect2.width = scaling_factor_*0.5;
//     rect2.height = scaling_factor_*0.5;
//     cv::Rect rect3;
//     rect3.x = scaling_factor_*(coc_viz.x-0.5);
//     rect3.y = scaling_factor_*(coc_viz.y);
//     rect3.width = scaling_factor_*0.5;
//     rect3.height = scaling_factor_*0.5;
//     cv::Rect rect4;
//     rect4.x = scaling_factor_*(coc_viz.x);
//     rect4.y = scaling_factor_*(coc_viz.y-0.5);
//     rect4.width = scaling_factor_*0.5;
//     rect4.height = scaling_factor_*0.5;
//     cv::rectangle(rsz_color_cop, rect1,  CV_RGB(255, 0, 0),3, 3);
//     cv::rectangle(rsz_color_cop, rect2,  CV_RGB(255, 0, 0),3, 3);
//     cv::rectangle(rsz_color_cop, rect3,  CV_RGB(255, 0, 0),3, 3);
//     cv::rectangle(rsz_color_cop, rect4,  CV_RGB(255, 0, 0),3, 3);
  */  
}


void VisFeats::show_windows(cv::Mat& img_show_feats)
{
  //display image/ 
  cv::imshow( "TI&F", img_show_feats );
  //    cv::resizeWindow("TI&F", cols*scaling_factor, rows*scaling_factor);
  
  //display image binary
  cv::Mat rsz_coc;
  cv::Mat bin_img_viz;
  cv::threshold(src_img_, bin_img_viz, coc_threshold_, 255, CV_THRESH_BINARY);
  cv::resize(bin_img_viz, rsz_coc, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
  cv::imshow("COC_binary", rsz_coc);
}


void VisFeats::cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1)
    {
        copx_des = msg_->control_features[0].centerpressure_x;
        copy_des = msg_->control_features[0].centerpressure_y;
        force_des = msg_->control_features[0].contactForce;
        cocx_des = msg_->control_features[0].centerContact_x;
        cocy_des = msg_->control_features[0].centerContact_y;
        orientz_des = msg_->control_features[0].contactOrientation;
	
	sizes2pixels(copx_des, copy_des, cop_viz_des);
	sizes2pixels(cocx_des, cocy_des, coc_viz_des);
    }
}

void VisFeats::sizes2pixels(float& x, float& y, cv::Point2d& ret_point)
{
  /*
  float copx = size_x_ - (float)((float)size_x_/
  ((float)rows_-1)) * ((float) (cop.y));
  
  float copy = size_y_ - (float)((float)size_y_/
  ((float)cols_-1)) * ((float) (cop.x));
  
  float cocx = size_x_ - (float)((float)size_x_/
  ((float)rows_-1)) * ((float) (coc.y));
  
  float cocy = size_y_ - (float)((float)size_y_/
  ((float)cols_-1)) * ((float) (coc.x));
*/
//   cop_viz.y = (size_x_ - copx_fb) * (rows_ - 1)/size_x_;
//   cop_viz.x = (size_y_ - copy_fb) * (cols_ - 1)/size_y_;
//   
//   coc_viz.y = (size_x_ - cocx_fb) * (rows_ - 1)/size_x_;
//   coc_viz.x = (size_y_ - cocy_fb) * (cols_ - 1)/size_y_;
  
  ret_point.y = (size_x_ - x) * (rows_ - 1)/size_x_;
  ret_point.x = (size_y_ - y) * (cols_ - 1)/size_y_;
} 

void VisFeats::cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1)
    {
        copx_fb = msg_->control_features[0].centerpressure_x;
        copy_fb = msg_->control_features[0].centerpressure_y;
        force_fb = msg_->control_features[0].contactForce;
        cocx_fb = msg_->control_features[0].centerContact_x;
        cocy_fb = msg_->control_features[0].centerContact_y;
        orientz_fb = msg_->control_features[0].contactOrientation;
	
	sizes2pixels(copx_fb, copy_des, cop_viz);
	sizes2pixels(cocx_des, cocy_des, coc_viz);
	
    }
}

void VisFeats::image_cb_viz(const sensor_msgs::ImageConstPtr& msg)
{
  uchar img[rows_][cols_];
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  
  int c = 0;
  total_pressure_ = 0;
  contact_points_n_ = 0;
  for(int i=0; i<rows_; ++i) {
    for(int j=0; j<cols_; j++) {
      img[i][j]=cv_ptr->image.data[c];
      total_pressure_ += img[i][j];
      c = c + 1;
      if (img[i][j] >= coc_threshold_)
      {
	contact_points_n_ = contact_points_n_ + 1;
      }
    }
  }
  src_img_ = cv::Mat(rows_, cols_, CV_8UC1, img);
}


  
