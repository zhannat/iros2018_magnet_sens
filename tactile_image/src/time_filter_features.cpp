#include <ros/ros.h>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/CalibWeissNano.h"
#include <geometry_msgs/WrenchStamped.h>

class FilterFeats

{
public:
  FilterFeats(int sensors_number);
  ~FilterFeats();

  ros::Subscriber sub_feats_fb;
  void callback_fbfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg);
  ros::NodeHandle nh;
  ros::Publisher pub_filtered;
  float copx_fb, copy_fb, force_fb,
  cocx_fb, cocy_fb, orientz_fb, zmp_x, zmp_y;
  bool is_feat_fb_rec_;
  
  tactile_servo_msgs::OneContFeats feats_buffer;
    tactile_servo_msgs::ContsFeats filterred_feats;

  int avg_smaples_;
  int counter_;
  
  void ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist, int& tactile_sensor_num);
  void get_sensor_params(int& tactile_sensor_num);


};

FilterFeats::FilterFeats(int sensors_number)
{
  sub_feats_fb = nh.subscribe("/fb_feats", 2,  &FilterFeats::callback_fbfeats,this);
  
  pub_filtered = nh.advertise<tactile_servo_msgs::ContsFeats>("/fb_feats_avg", 2);
  is_feat_fb_rec_ = false;
  get_sensor_params(sensors_number);
  counter_ = 1;
  copx_fb = 0;
  copy_fb = 0;
  force_fb = 0;
  cocx_fb = 0;
  cocy_fb = 0;
  orientz_fb = 0;
  zmp_x = 0;
  zmp_y = 0;
}
FilterFeats::~FilterFeats()
{}

void FilterFeats::callback_fbfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if ((msg->control_features.size() == 1)&&(counter_ <= avg_smaples_))
  {
    copx_fb = copx_fb + msg->control_features[0].centerpressure_x;
    copy_fb = copy_fb + msg->control_features[0].centerpressure_y;
    force_fb = force_fb + msg->control_features[0].contactForce;
    cocx_fb = cocx_fb + msg->control_features[0].centerContact_x;
    cocy_fb = cocy_fb + msg->control_features[0].centerContact_y;
    orientz_fb = orientz_fb + msg->control_features[0].contactOrientation;
    zmp_x = zmp_x + msg->control_features[0].zmp_x;
    zmp_y = zmp_y + msg->control_features[0].zmp_y;
    counter_ = counter_ + 1;
    is_feat_fb_rec_ = false;
    // 	ROS_FATAL_STREAM("fb feat  received");
  }
  else if ((msg->control_features.size() == 1)&&(counter_ > avg_smaples_))
  {
    copx_fb = copx_fb/counter_;
    copy_fb = copy_fb/counter_;
    force_fb = force_fb/counter_;
    cocx_fb  = cocx_fb/counter_;
    cocy_fb = cocy_fb/counter_;
    orientz_fb = orientz_fb/counter_;
    zmp_x = zmp_x/counter_;
    zmp_y = zmp_y/counter_;
    feats_buffer.centerContact_x = cocx_fb;
    feats_buffer.centerContact_y = cocy_fb;
    feats_buffer.centerpressure_x = copx_fb;
    feats_buffer.centerpressure_y = copy_fb;
    feats_buffer.contactForce = force_fb;
    feats_buffer.zmp_x = zmp_x;
    feats_buffer.zmp_y = zmp_y;
    feats_buffer.contactOrientation = orientz_fb;
    
    filterred_feats.header.frame_id = "weiss_filtered_fb";
    filterred_feats.header.stamp = ros::Time::now();
    filterred_feats.control_features.push_back(feats_buffer);
    counter_ = 1;
    is_feat_fb_rec_ = true;

  }
  else
  {
    is_feat_fb_rec_ = false;
    ROS_FATAL_STREAM("fb feat is not received");
  }
  if (  is_feat_fb_rec_ == true ) 
  {
    pub_filtered.publish(filterred_feats);
    filterred_feats.control_features.clear();
  }
}

void FilterFeats::get_sensor_params(int& tactile_sensor_num){

    XmlRpc::XmlRpcValue my_list;
    //get tactile sensor's parameters from param server
    nh.getParam("/tactile_arrays_param", my_list);
    ckeck_param_fields(my_list, tactile_sensor_num);
    avg_smaples_ = static_cast<int> (my_list[tactile_sensor_num]["features_filtering_samples"]);
}

void FilterFeats::ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist, int& tactile_sensor_num)
{
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["features_filtering_samples"].getType() == XmlRpc::XmlRpcValue::TypeInt, "no avg smaples");
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "filter_feats");
    ros::NodeHandle n;
    ros::Rate loop_rate(125);
    ROS_INFO("filter_feats");
    FilterFeats weiss_filter(0);
    while( ros::ok() )
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


